# encoding: UTF-8
# Setup first superadministrator account
admin = User.create({:email => 'jantosovic.martin@gmail.com', :password => 'administrator', :password_confirmation => 'administrator'})
role = Role.create({:name => 'superadministrator', :description => 'Super administrátor je užívateľ, ktorý môže spravovať všetko.'})
all_permissions = Permission.find(1)
role.permissions << all_permissions
admin.roles << role
