class CreatePermissions < ActiveRecord::Migration
  def self.up
    create_table :permissions do |t|
      t.string :position
      t.string :name,           :null => false
      t.string :description
      t.string :action,         :null => false
      t.string :subject_class,  :null => false
      t.integer :subject_id

      t.timestamps
    end

    create_table :permissions_roles, :id => false do |t|
      t.integer :permission_id
      t.integer :role_id
    end
  end

  def self.down
    drop_table :permissions
    drop_table :permissions_roles
  end
end
