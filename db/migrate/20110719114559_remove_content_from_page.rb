class RemoveContentFromPage < ActiveRecord::Migration
  def self.up
    remove_column :pages, :content
    rename_column :references, :short_content, :content
  end

  def self.down
    add_column :pages, :content, :text
    rename_column :references, :content, :short_content
  end
end
