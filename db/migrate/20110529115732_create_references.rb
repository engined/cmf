class CreateReferences < ActiveRecord::Migration
  def self.up
    create_table :references do |t|
      t.string :city
      t.text :short_content
      t.string :image
    end
  end

  def self.down
    drop_table :references
  end
end
