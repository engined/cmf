class CreatePages < ActiveRecord::Migration
  def self.up
    create_table :pages do |t|
      t.references :user, :null => false
      t.string :title, :null => false
      t.string :alias
      t.text :content
      t.text :meta_description
      t.text :meta_keywords
      t.string :meta_robots
      t.string :meta_author
      t.boolean :published, :default => true
      t.datetime :published_from
      t.datetime :published_to

      t.references :pageable, :polymorphic => true

      t.integer :showed_count, :default => 0, :null => false
      t.integer :modified_count, :default => 0, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :pages
  end
end
