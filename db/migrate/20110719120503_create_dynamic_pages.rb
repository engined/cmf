class CreateDynamicPages < ActiveRecord::Migration
  def self.up
    create_table :dynamic_pages do |t|
      t.text :content
    end
  end

  def self.down
    drop_table :dynamic_pages
  end
end
