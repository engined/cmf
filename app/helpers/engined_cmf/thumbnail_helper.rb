module EnginedCmf::ThumbnailHelper

  HOME = 'uploads'

  EMPTY_GIF = "GIF89a\x01\x00\x01\x00\x80\x00\x00\x00\x00\x00\x00\x00\x00!\xf9\x04\x01\x00\x00\x00\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00;"

  PROCESS_RESIZE = 1
  PROCESS_RESIZE_TO_FILL = 2
  PROCESS_RESIZE_TO_FIT = 3

  UPLOADED_DIR = File.join(Rails.root, 'public', HOME)
  CACHE_DIR = File.join(Rails.root, 'public', 'images', 'cache')

  def get_thumbnail_uri(path, width, height, process)
    path = get_thumbnail_path(path, width, height, process)
    path.sub(/#{File.join(Rails.root, 'public')}/, '')
  end

  def get_thumbnail_path(path, width, height, process)
    File.join(CACHE_DIR, get_thumbnail_name(path, width, height, process))
  end

  def get_thumbnail_name(path, width, height, process)
    if ! FileTest.exists?(path)
      'empty.gif'
    else
      hash = Digest::MD5.hexdigest(path + '|' + File.mtime(path).to_i.to_s)
      suffix = File.extname(path)
      "preview-#{width}-#{height}-#{process}-#{hash}#{suffix}"
    end
  end

  def get_full_path(path)
    File.join(UPLOADED_DIR, path)
  end

  def thumbnail_image_path(url, width, height, process, helper = nil)
    if url !~ /.*\/#{HOME}\/(.*)/
      return url
    end

    path = url.sub(/.*\/#{HOME}\/(.*)/, '\1')
    full_path = get_full_path(path)
    thumbnail = get_thumbnail_path(full_path, width, height, process)
    if FileTest.exists?(thumbnail)
      get_thumbnail_uri(full_path, width, height, process)
    else
      helper_params = { :path => path, :width => width, :height => height, :process => process }
      if helper
        send helper, helper_params
      else
        if defined? thumbnail_path
          thumbnail_path(helper_params)
        else
          admin_thumbnail_path(helper_params)
        end
      end
    end
  end

end
