# encoding: UTF-8
module EnginedCmf::ApplicationHelper

  def title(value, variables = {})
    content_for(:title) { t value, variables }
  end

  def yield_for(symbol, default)
    output = content_for(symbol)
    output = default if output.blank?
    output
  end

  def roles_link_list(user)
    roles = []
    user.roles.each do |role|
      if can? :read, role
        roles << link_to(admin_role_path(role), :title => t('K roli')) do
          role.name
        end
      else
        roles << role.name
      end
    end
    roles.join(', ')
  end

  def author_link(author)
    if can? :read, author
      link_to(admin_user_path(author), :title => t('K užívateľovi')) do
        author.fullname
      end
    else
      author.fullname
    end
  end

  def link_to_remove_fields(name, f, *args)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)", *args)
  end

  def link_to_add_fields(name, f, association, *args)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(args[0][:file].nil? ? association.to_s.singularize + "_fields" : args[0][:file], :f => builder)
    end
    args[0][:file] = nil if args[0][:file]
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", *args)
  end

end
