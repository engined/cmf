class EnginedCmf::FilesController < EnginedCmf::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => ['elfinder']

  def index
    authorize! :read, File
  end

  def elfinder
    h, r = ElFinder::Connector.new(
      :root => File.join(Rails.public_path, 'uploads'),
      :allow_dot_files => false,
      :url => '/uploads',
       :perms => {
         /^\.[^\/]/ => { :read => false, :write => false, :rm => false },
         '.' => { :read => can?(:read, File), :write => can?(:write, File), :rm => can?(:destroy, File) }, # '.' is the proper way to specify the home/root directory.
         /^.*$/ => { :read => can?(:read, File), :write => can?(:write, File), :rm => can?(:destroy, File) }, # '.' is the proper way to specify the home/root directory.
       },
       :extractors => {
         'application/zip' => ['unzip', '-qq', '-o'], # Each argument will be shellescaped (also true for archivers)
         'application/x-gzip' => ['tar', '-xzf'],
       },
       :archivers => {
         'application/zip' => ['.zip', 'zip', '-qr9'], # Note first argument is archive extension
         'application/x-gzip' => ['.tgz', 'tar', '-czf'],
        }

    ).run(params)
    headers.merge!(h)
    render (r.empty? ? {:nothing => true} : {:text => r.to_json}), :layout => false
  end

  # CanCan permissions setup
  # Use rake engined_cmf:fill_permissions for filling database with controller
  # permissions. Run this command before db:seed
  def self.permissions
    return :file, "Files manager", [
      [ :read, "View files", "User can view all files in the system.", 1 ],
      [ :write, "Write files and directories", "User can write new files or create directories in the system.", 2],
      [ :destroy, "Remove files", "User can remove files or directories from the system.", 3 ]
    ]
  end

end
