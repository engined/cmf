class EnginedCmf::Devise::SessionsController < Devise::SessionsController
  include EnginedCmf::DeviseHelper

  layout :get_layout
end
