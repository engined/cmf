class EnginedCmf::Devise::PasswordsController < Devise::PasswordsController
  include EnginedCmf::DeviseHelper

  layout :get_layout
end
