class EnginedCmf::Devise::UnlocksController < Devise::UnlocksController
  include EnginedCmf::DeviseHelper

  layout :get_layout
end
