class EnginedCmf::RolesController < EnginedCmf::ApplicationController
  inherit_resources

  defaults :route_prefix => :admin

  load_and_authorize_resource

  def update
    params[:role][:permission_ids] ||= []
    super
  end

  protected

  # CanCan permissions setup
  # Use rake engined_cmf:fill_permissions for filling database with controller
  # permissions. Run this command before db:seed
  def self.permissions
    return :role, "Roles manager", [
      [ :read, "View roles", "User can view all roles in the system.", 1 ],
      [ :create, "Create new role", "User can create new roles in the system.", 2 ],
      [ :update, "Edit role", "User can edit roles in the system.", 3 ],
      [ :destroy, "Remove role", "User can remove roles from the system.", 4 ]
    ]
  end
end
