class EnginedCmf::ReferencesController < EnginedCmf::ApplicationController
  inherit_resources

  defaults :route_prefix => :admin

  load_and_authorize_resource

  def create
    @reference.page.user_id = current_user.id
    create! { admin_references_url }
  end

  def update
    update! { admin_references_url }
  end

  # CanCan permissions setup
  # Use rake engined_cmf:fill_permissions for filling database with controller
  # permissions. Run this command before db:seed
  def self.permissions
    return :reference, "References manager", [
      [ :read, "View references", "User can view all references in the system.", 1 ],
      [ :create, "Create new reference", "User can create new references in the system.", 2 ],
      [ :update, "Edit reference", "User can edit references in the system.", 3 ],
      [ :destroy, "Remove reference", "User can remove references from the system.", 4 ]
    ]
  end

end
