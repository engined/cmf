class EnginedCmf::ImagesController < EnginedCmf::ApplicationController
  inherit_resources

  defaults :route_prefix => :admin

  actions :index, :new, :create, :destroy

  belongs_to :reference, :polymorphic => true
end
