class EnginedCmf::ProfileController < EnginedCmf::ApplicationController

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        sign_in User, @user, :bypass => true
        set_flash_message :notice, :updated
        format.html { redirect_to(admin_profile_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end

  end

end
