require 'RMagick'

class EnginedCmf::ThumbnailController < ApplicationController

  include EnginedCmf::ThumbnailHelper

  def show
    path = get_full_path(params[:path])
    thumb_path = get_thumbnail_path(path, params[:width], params[:height], params[:process])

    if FileTest.exists?(thumb_path)
      image = Magick::Image.read(thumb_path).first
    else
      image = generate(path, params[:width], params[:height], params[:process])
    end

    expires_in 14.days
    send_data(image.to_blob, :disposition => 'inline', :type => image.mime_type)
  end

  private

  def generate(path, width, height, process)
    process = process.to_i
    width = width.to_i
    height = height.to_i

    begin
      image = Magick::Image.read(path).first

      if process ==  PROCESS_RESIZE
        image.resize! width, height
      elsif process == PROCESS_RESIZE_TO_FILL
        image.resize_to_fill! width, height
      elsif process == PROCESS_RESIZE_TO_FIT
        image.resize_to_fit! width, height
      end

      thumb_path = get_thumbnail_path(path, params[:width], params[:height], params[:process])
      image.write(thumb_path)
      File.chmod(0666, thumb_path)
    rescue
      image = Magick::Image.from_blob(EMPTY_GIF).first
    end

    image
  end

end
