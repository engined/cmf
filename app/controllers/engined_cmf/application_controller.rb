class EnginedCmf::ApplicationController < ApplicationController
  unloadable

  before_filter :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    sign_out current_user
    redirect_to user_root_url, :alert => exception.message
  end

  layout 'engined_cmf/application.html.haml'

  # Sets the flash message with :key, using I18n. By default you are able
  # to setup your messages using specific resource scope, and if no one is
  # found we look to default scope.
  # Example (i18n locale file):
  #
  #   en:
  #     devise:
  #       passwords:
  #         #default_scope_messages - only if resource_scope is not found
  #         user:
  #           #resource_scope_messages
  #
  # Please refer to README or en.yml locale file to check what messages are
  # available.
  def set_flash_message(key, kind, options={}) #:nodoc:
    options[:scope] = "engined_cmf.#{controller_name}"
    options[:default] = Array(options[:default]).unshift(kind.to_sym)
    message = I18n.t("#{kind}", options)
    flash[key] = message if message.present?
  end

end
