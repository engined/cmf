# encoding: UTF-8
class EnginedCmf::IndexController < EnginedCmf::ApplicationController

  prepend_before_filter :redirect_unsigned

  def index
    @menu = [
      {
        :class => 'references',
        :selected => can?(:read, Reference),
        :image => 'engined_cmf/icon-references.png',
        :title => I18n.t('Referencie'),
        :description => I18n.t('Pridajte, vymažte alebo pozmeňte Vaše referencie'),
        :link => admin_references_path
      },
      {
        :class => 'pages',
        :selected => can?(:read, DynamicPage),
        :image => 'engined_cmf/icon-pages.png',
        :title => I18n.t('Stránky'),
        :description => I18n.t('Pridajte, vymažte alebo pozmeňte Vaše stránky'),
        :link => admin_dynamic_pages_path
      },
      {
        :class => 'files',
        :selected => can?(:read, File),
        :image => 'engined_cmf/icon-files.png',
        :title => I18n.t('Správa súborov'),
        :description => I18n.t('Nahrajte na server obrázky alebo iné súbory'),
        :link => admin_files_path
      },
      {
        :class => 'users',
        :selected => can?(:read, User),
        :image => 'engined_cmf/icon-users.png',
        :title => I18n.t('Užívatelia'),
        :description => I18n.t('Vytvorte účty pre viacerých ľudí a nastavte ich práva'),
        :link => admin_users_path
      },
      {
        :class => 'roles',
        :selected => can?(:read, Role),
        :image => 'engined_cmf/icon-roles.png',
        :title => I18n.t('Užívateľské role'),
        :description => I18n.t('Prideľte uživateľom rôzne práva'),
        :link => admin_roles_path
      },
    ]

    @grouped_menu = @menu.in_groups_of(4)
  end

  private

  def redirect_unsigned
    unless current_user
      redirect_to new_user_session_path
    end
  end

end
