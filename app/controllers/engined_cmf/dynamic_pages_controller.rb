class EnginedCmf::DynamicPagesController < EnginedCmf::ApplicationController
  inherit_resources

  defaults :route_prefix => :admin

  load_and_authorize_resource

  def create
    @dynamic_page.page.user_id = current_user.id
    create! { admin_dynamic_pages_url }
  end

  def update
    update! { admin_dynamic_pages_url }
  end

  # CanCan permissions setup
  # Use rake engined_cmf:fill_permissions for filling database with controller
  # permissions. Run this command before db:seed
  def self.permissions
    return :dynamic_page, "Pages manager", [
      [ :read, "View pages", "User can view all pages in the system.", 1 ],
      [ :create, "Create new page", "User can create new page in the system.", 2 ],
      [ :update, "Edit page", "User can edit pages in the system.", 3 ],
      [ :destroy, "Remove page", "User can remove pages from the system.", 4 ]
    ]
  end

end
