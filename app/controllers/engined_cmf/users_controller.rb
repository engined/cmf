class EnginedCmf::UsersController < EnginedCmf::ApplicationController
  inherit_resources
  defaults :route_prefix => :admin

  load_and_authorize_resource

  def create
    create! { collection_url }
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
    end

    respond_to do |format|
      params[:user][:role_ids] ||= []
      if @user.update_attributes(params[:user])
        if current_user.id == @user.id
          sign_in User, @user, :bypass => true
        end
        set_flash_message :notice, :updated, { :fullname => @user.fullname }
        format.html { redirect_to(collection_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # CanCan permissions setup
  # Use rake engined_cmf:fill_permissions for filling database with controller
  # permissions. Run this command before db:seed
  def self.permissions
    return :user, "Users manager", [
      [ :read, "View users", "User can view all users in the system.", 1 ],
      [ :create, "Create new user", "User can create new users in the system.", 2 ],
      [ :update, "Edit user", "User can edit users in the system.", 3 ],
      [ :destroy, "Remove user", "User can remove users from the system.", 4 ]
    ]
  end

  protected

  def resource
    @user ||= User.includes(:roles).find(params[:id])
  end

  def collection
    @users ||= User.includes(:roles).all
  end

end
