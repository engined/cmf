class User < ActiveRecord::Base

  has_and_belongs_to_many :roles, :include => :permissions
  has_many :pages

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :registerable, :timeoutable and :omniauthable
  devise :database_authenticatable, :lockable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :role_ids

  has_friendly_id :fullname, :use_slug => true, :approximate_ascii => true

  def fullname
    if first_name.blank? and last_name.blank?
      "#{email}"
    else
      "#{first_name} #{last_name}"
    end
  end

  def get_roles
    roles.collect(&:name).join(', ')
  end

  def permissions(actions = nil)
    @permissions = Permission.includes(:roles).find(:all, :conditions => { :roles => { :id => roles }}).to_a

    if actions.nil?
      @permissions
    else
      @permissions.select { |perm| actions.include? perm.action.downcase.to_sym }
    end
  end

  def can_delete?
    pages.empty?
  end

end
