class DynamicPage < ActiveRecord::Base

  has_one :page, :as => :pageable, :autosave => true, :dependent => :destroy

  accepts_nested_attributes_for :page

  has_friendly_id :alias, :use_slug => true, :approximate_ascii => true

  attr_accessible :content, :page_attributes

  validates_presence_of :content

  def initialize(attributes = nil)
    self.page = Page.new
    super(attributes)
  end

  def title
    page.title
  end

  def alias
    page.alias
  end

end
