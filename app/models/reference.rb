class Reference < ActiveRecord::Base
  has_many :images, :as => :imageable, :autosave => true, :dependent => :destroy
  has_one :page, :as => :pageable, :autosave => true, :dependent => :destroy

  accepts_nested_attributes_for :page
  accepts_nested_attributes_for :images, :allow_destroy => true, :reject_if => lambda { |i| i[:url].blank? }

  attr_accessible :city, :content, :page_attributes, :images_attributes, :image

  has_friendly_id :alias, :use_slug => true, :approximate_ascii => true

  validates_presence_of :city, :content

  def initialize(attributes = nil)
    self.page = Page.new
    super(attributes)
  end

  def title
    page.title
  end

  def alias
    page.alias
  end

end
