class Permission < ActiveRecord::Base
  has_and_belongs_to_many :roles

  validates_presence_of :name
  validates_presence_of :action
  validates_presence_of :subject_class

  def self.find_all
    order("subject_class ASC, position ASC")
  end

  def self.find_all_for_role
    find_all.group_by(&:subject_class)
  end
end
