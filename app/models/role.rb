class Role < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_and_belongs_to_many :permissions

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :description, :permission_ids

  validates_presence_of :name

  has_friendly_id :name, :use_slug => true, :approximate_ascii => true

  def has_permission?(permission)
    permissions.any? do |perm|
      perm.id ==  permission.id
    end
  end
end
