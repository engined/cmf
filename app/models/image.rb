class Image < ActiveRecord::Base
  belongs_to :imageable, :polymorphic => true

  attr_accessible :name, :url, :_destroy

  validates_presence_of :name, :url
end
