class Page < ActiveRecord::Base
  belongs_to :pageable, :polymorphic => true
  belongs_to :user

  # Setup accessible (or protected) attributes for your model
  attr_protected :showed_count, :modified_count, :user_id, :pageable_id, :pageable_type

  validates_presence_of :title, :user_id

  def published_to_formatted=(value)
    self.published_to = value
  end

  def published_to_formatted
    if self.published_to
      I18n.l(self.published_to.to_date)
    else
      ''
    end
  end

  def published_from_formatted=(value)
    self.published_from = value
  end

  def published_from_formatted
    if self.published_from
      I18n.l(self.published_from.to_date)
    else
      ''
    end
  end

  def published?
    today = Time.now
    self.published &&
      (self.published_from.nil? || today > self.published_from) &&
      (self.published_to.nil? || today < self.published_to)
  end

end
