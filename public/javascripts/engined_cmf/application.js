$(function() {

	$('a.file-selector').click(function() {
		var filter = $(this).data('filter');
		if (filter)
			filter = new RegExp(filter, 'i');
		var filterMessage = $(this).data('filter-message');
		var input = $(this).siblings('input');
		var el = $('<div/>').elfinder({
		  url: admin_elfinder_path(),
		  dialog: { width: 900, modal: true, title: 'Files', zIndex: 400001 },
		  closeOnEditorCallback: false,
		  editorCallback: function(url) {

			if (filter && !filter.exec(url))
				alert(filterMessage);
			else {
				this.dialog.close();
				input.val(url);
			}

		  }
		})
	})

	// Date pickers
	$("input.datepicker").each(function() {
		$(this).datepicker().each(function(index, value) {
			if ($(this).data('datepicker-from-now')) {
				$(this).one("click", function() {
					$(this).datepicker("option", "minDate", new Date());
				});
			}

			if ($(this).data("datepicker-until")) {
				var until = $(this).data("datepicker-until")
				$(this).datepicker("option", "maxDate", until);
			}
		});
	});

	// Date pickers
	$("input.datetimepicker").each(function() {
		$(this).datetimepicker().each(function(index, value) {
			if ($(this).attr('data-datetimepicker-from-now')) {
				$(this).one("click", function() {
					$(this).datetimepicker("option", "minDate", new Date());
				});
			}

			if ($(this).data("datepicker-until")) {
				var until = $(this).data("datepicker-until")
				$(this).datetimepicker("option", "maxDate", until);
			}
		});
	});

	$("a.page-alias-generate").click(function(event) {
		var values = new Array;
		$("input.page-title").each(function() {
			if ($(this).val() != "")
				values.push($(this).val());
		});
		var value = values.join(' ');
		value = value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').toLowerCase();

		var replacements = {
			'a':        /[\u0105\u0104\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5]/g,
			'c':        /[\u00E7\u010D\u0107\u0106]/g,
			'd':        /[\u010F]/g,
			'e':        /[\u00E8\u00E9\u00EA\u00EB\u011B\u0119\u0118]/g,
			'i':        /[\u00EC\u00ED\u00EE\u00EF]/g,
			'l':        /[\u0142\u0141]/g,
			'n':        /[\u00F1\u0148]/g,
			'o':        /[\u00F2\u00F3\u00F4\u00F5\u00F6\u00F8\u00D3]/g,
			'r':        /[\u0159]/g,
			's':        /[\u015B\u015A\u0161]/g,
			'ss':       /[\u00DF]/g,
			't':        /[\u0165]/g,
			'u':        /[\u00F9\u00FA\u00FB\u00FC\u016F]/g,
			'y':        /[\u00FD\u00FF]/g,
			'z':        /[\u017C\u017A\u017B\u0179\u017E]/g,
			'ae':       /[\u00E6]/g,
			'oe':       /[\u0153]/g,
			'l':        /[\u013E\u013A]/g,
			'r':        /[\u0155]/g,
			'':         /[^a-z0-9\s\'\:\/\[\]-]/g,
			' ':        /[\s\'\:\/\[\]-]+/g,
			'-':        /[ \/]/g
		}

		$.each(replacements, function(replacement, regex) {
			value = value.replace(regex, replacement);
		});

		$("input.page-alias").val(value);

	});


});
