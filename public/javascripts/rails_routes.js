function new_user_session_path(params){ return '/admin/auth/login'}
function user_session_path(params){ return '/admin/auth/login'}
function destroy_user_session_path(params){ return '/admin/auth/logout'}
function user_password_path(params){ return '/admin/auth/password'}
function new_user_password_path(params){ return '/admin/auth/password/new'}
function edit_user_password_path(params){ return '/admin/auth/password/edit'}
function user_unlock_path(params){ return '/admin/auth/unlock'}
function new_user_unlock_path(params){ return '/admin/auth/unlock/new'}
function admin_root_path(params){ return '/admin'}
function admin_profile_path(params){ return '/admin/profile'}
function admin_files_path(params){ return '/admin/files'}
function admin_elfinder_path(params){ return '/admin/files/elfinder'}
function admin_users_path(params){ return '/admin/users'}
function new_admin_user_path(params){ return '/admin/users/new'}
function edit_admin_user_path(params){ return '/admin/users/' + params.id + '/edit'}
function admin_user_path(params){ return '/admin/users/' + params.id + ''}
function admin_roles_path(params){ return '/admin/roles'}
function new_admin_role_path(params){ return '/admin/roles/new'}
function edit_admin_role_path(params){ return '/admin/roles/' + params.id + '/edit'}
function admin_role_path(params){ return '/admin/roles/' + params.id + ''}
function admin_references_path(params){ return '/admin/references'}
function new_admin_reference_path(params){ return '/admin/references/new'}
function edit_admin_reference_path(params){ return '/admin/references/' + params.id + '/edit'}
function admin_reference_path(params){ return '/admin/references/' + params.id + ''}
function root_path(params){ return '/'}
function rails_info_properties_path(params){ return '/rails/info/properties'}
