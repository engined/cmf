# encoding: UTF-8
SimpleNavigation::Configuration.run do |navigation|

  navigation.items do |primary|

    primary.dom_class = 'main-menu'

    primary.item :home, I18n.t('Home'), admin_root_path
    primary.item :references, I18n.t('References'), admin_references_path, :highlights_on => /#{admin_references_path}/, :if => Proc.new { can?(:read, Reference) } do |sub|
      sub.item :new_reference, I18n.t('New reference'), new_admin_reference_path, :if => Proc.new { can?(:create, Reference) }
      #sub.item :edit_reference, I18n.t('Edit reference'), edit_admin_reference_path, :if => Proc.new { can? :update, Reference }
    end
    primary.item :pages, I18n.t('Pages'), admin_dynamic_pages_path, :highlights_on => /#{admin_dynamic_pages_path}/, :if => Proc.new { can?(:read, DynamicPage) } do |sub|
      sub.item :new_dynamic_page, I18n.t('New page'), new_admin_dynamic_page_path, :if => Proc.new { can?(:create, DynamicPage) }
      #sub.item :edit_reference, I18n.t('Edit reference'), edit_admin_reference_path, :if => Proc.new { can? :update, Reference }
    end
    primary.item :files, I18n.t('Files'), admin_files_path, :highlights_on => /#{admin_files_path}/, :if => Proc.new { can?(:read, File) }
    primary.item :users, I18n.t('Users'), admin_users_path, :highlights_on => /#{admin_users_path}/, :if => Proc.new { can?(:read, User) } do |sub|
      sub.item :new_user, I18n.t('New user'), new_admin_user_path, :if => Proc.new { can?(:create, User) }
    end
    primary.item :roles, I18n.t('Roles'), admin_roles_path, :highlights_on => /#{admin_roles_path}/, :if => Proc.new { can?(:read, Role) } do |sub|
      sub.item :new_role, I18n.t('New role'), new_admin_role_path, :if => Proc.new { can?(:create, Role) }
    end

  end

end
