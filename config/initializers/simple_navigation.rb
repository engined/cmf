if FileTest.exists? Rails.root + 'vendor/plugins/engined-cmf/lib/engined_cmf/navigation_renderers/breadcrumbs_renderer.rb'
  require Rails.root + 'vendor/plugins/engined-cmf/lib/engined_cmf/navigation_renderers/breadcrumbs_renderer'
else
  require Rails.root + 'lib/engined_cmf/navigation_renderers/breadcrumbs_renderer'
end

SimpleNavigation.config_file_path = File.join(Rails.root, 'config', 'navigations')
SimpleNavigation.register_renderer :engined_cmf_breadcrumbs => EnginedCmf::NavigationRenderers::BreadcrumbsRenderer
