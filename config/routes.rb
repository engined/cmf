EnginedCmf::Application.routes.draw do

  scope '/admin' do
    devise_for :users, :path_names => { :sign_in => 'login', :sign_out => 'logout' }, :path => 'auth',
      :controllers => { :sessions => 'engined_cmf/devise/sessions', :passwords => 'engined_cmf/devise/passwords', :unlocks => 'engined_cmf/devise/unlocks' }
  end

  namespace :admin, :module => 'engined_cmf' do
    get '/' => 'index#index', :as => 'root'
    get '/profile' => 'profile#edit', :as => 'profile'
    put '/profile' => 'profile#update'

    get '/files' => 'files#index', :as => 'files'
    match '/files/elfinder' => 'files#elfinder', :as => 'elfinder'

    resources :users, :roles, :dynamic_pages
    resources :references do
      resources :images, :except => [ :show, :edit, :update ]
    end

    match '/thumbnail/:width/:height/:process/*path' => 'thumbnail#show',
      :as => 'thumbnail',
      :constraints => { :width => /\d+/, :height => /\d+/, :process => /\d+/ }
  end

  root :to => 'index#index'
end
