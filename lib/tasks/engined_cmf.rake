namespace :engined_cmf do

  desc "Scan all CMF Engine controllers and fill the permissions database table"
  task :fill_permissions => :environment do
    puts "Starting to scan EnginedCmf::* controllers..."
    setup_permissions
    puts "Done!"
  end

  desc "Sync all necessary files required for running Engined CRM as plugin"
  task :sync do
    system "rsync -ruv vendor/plugins/engined-cmf/db/migrate db"
    system "rsync -ruv vendor/plugins/engined-cmf/public ."
    system "rsync -ruv vender/plugins/engined-cmf/config/initializers/simple_navigation.rb config/initializers/simple_navigation.rb"
  end

end

#-------------------------------------------------------------------------------
# UTILS
#-------------------------------------------------------------------------------

def setup_permissions

  all_permissions = []

  all_permissions << write_permission("all", "manage", "Everything", "All operations", 0, true)

  controllers = Dir.new("#{RAILS_ROOT}/app/controllers/engined_cmf").entries
  controllers.each do |controller|
    if controller =~ /_controller/
      foo_bar = ("EnginedCmf::" + controller.camelize.gsub(".rb","")).constantize.new
    end
  end

  # You can change ApplicationController for a super-class used by your restricted controllers
  EnginedCmf::ApplicationController.subclasses.each do |controller|
    if controller.respond_to?(:permissions)
      clazz, description, permissions = controller.permissions
      all_permissions << write_permission(clazz, "manage", description, "All operations")
      permissions.each do |permission|
        action, name, description, position = permission
        all_permissions << write_permission(clazz, action, name, description, position)
      end
    end
  end

  # Cleanup
  actual = Permission.find(:all)
  to_remove = actual - all_permissions
  to_remove.each(&:destroy)

end

def write_permission(class_name, cancan_action, name, description, position = 0, force_id_1 = false)
  permission  = Permission.find(:first, :conditions => ["subject_class = ? and action = ?", class_name, cancan_action])
  if not permission
    puts "Writing permission for subject '#{class_name}' on '#{cancan_action}' action"
    permission = Permission.new
    permission.id = 1 unless not force_id_1
    permission.position = position
    permission.name = name
    permission.description = description
    permission.subject_class =  class_name
    permission.action = cancan_action
    permission.save
  else
    permission.position = position
    permission.name = name
    permission.description = description
    permission.save
  end

  return permission
end
