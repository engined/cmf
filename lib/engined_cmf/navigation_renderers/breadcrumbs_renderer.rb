module EnginedCmf
  module NavigationRenderers
    class BreadcrumbsRenderer < SimpleNavigation::Renderer::Base

      def render(item_container)
        content_tag(:ul, li_tags(item_container).join(''), {:id => container_id, :class => container_class })
      end

      protected

      def li_tags(item_container)
        item_container.items.inject([]) do |list, item|
          if item.selected?
            list << content_tag(:li, link_to(item.name, item.url, {:method => item.method}.merge(item.html_options.except(:class,:id)))) if item.selected?
            if include_sub_navigation?(item)
              list.concat li_tags(item.sub_navigation)
            end
          end
          list
        end
      end

      def container_id
        @container_id ||= options[:container_id] || nil
      end

      def container_class
        @container_class ||= options[:container_class] || nil
      end
    end
  end
end
